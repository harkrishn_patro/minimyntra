var express = require('express');
var superagent = require('superagent');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Myntra' });
});

function getDataAndDisplay(urlToHit,cb) {
  superagent.get(urlToHit, function(err,doc) {
    if(err) {
      console.log(err);
    }
    cb(doc.body);
  });

}

router.post('/search', function(req,res) {
  var search = req.body.search;
  res.redirect('/search/'+search);
});

router.get('/search/:item', function(req,res) {
  var searchQuery = req.params.item;
  var urlToHit = 'http://developer.myntra.com/search/data/' + searchQuery;
  console.log(urlToHit);

  getDataAndDisplay(urlToHit, function (response) {
    console.log(response.data.results.products);

    res.render('searchpage', {
      'data': response.data.results.products
    });
  });
});

router.get('/product/:item', function(req,res) {
  var searchQuery = req.params.item;
  var urlToHit = 'http://developer.myntra.com/style/' + searchQuery;
  getDataAndDisplay(urlToHit,function(response) {
    console.log(response.data);
    res.render('pdp',{
      'data': response.data
    });
  });
});

router.get('/bought', function(req, res, next) {
  res.render('productbought');
});


module.exports = router;
